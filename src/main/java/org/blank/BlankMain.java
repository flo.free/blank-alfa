package org.blank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class BlankMain {

	private final static Logger logger = LoggerFactory.getLogger(BlankMain.class);

	public static void main(String[] args) {
		String from = ", from Italy";
		logger.info("Hello World{}!", from);
	}

}
